import { Component } from "react";

import Button from '../../Button'; 
import {ReactComponent as Favorite} from './icons/favorites.svg';
import {ReactComponent as FavActive} from './icons/favActive.svg';

class Card extends Component {
    render(){
        const {
            name,
            price,
            img,
            articul,
            color,
            inFav,
        } = this.props.data;
         const className = "icon-favoriteCards";
        return(
           <>
           
            <div className={"cards__card"}  >
                <span className={className} onClick={(event) => {this.props.handlerFavorite(this.props.data); event.stopPropagation();}}>
                {inFav ? <FavActive  /> : <Favorite/>}
                </span>
                <p className="card__data">{name} <br/> {price}</p>
                <div className="card__icon">
                    <img className="card__img" src={img} alt={articul}/>
                </div>
            <span className="card__temp">{color}</span>
            <Button handlerClick={()=> {
                this.props.handlerModal();
                this.props.handlerCurrentProd(this.props.data)
            }}/>
            </div>
            </>
        )
    }
}

export default Card;