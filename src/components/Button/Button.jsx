import {Component} from "react";

import {ReactComponent as Cart} from './components/icons/cart.svg';
import './Button.scss';

class Button extends Component{
    render(){
        const {handlerModal, handlerCurrentProd} = this.props; 
        return(
       
            <div className="wrapper">
                <button className ="button" type="button" onClick={this.props.handlerClick}>
                    <span className="button--text">
                        Buy
                    </span>
                    <Cart/>
                    
                </button>
            </div>
           
       )
    }
}
export default Button;
