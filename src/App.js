import React, { Component } from 'react';


import Header from './components/Header';
import { Modal } from './components/Modal';
import AllCards from './components/AllCards';
import Footer from './components/Footer/Footer';

class App extends Component {
  state = {
    isModal: false,
    cards: [],
    cart: [],
    currentProd: {},
    favorite: []
  }
  componentDidMount() {
    fetch("phones.json")
      .then((response) => response.json())
      .then((data) => {
        this.setState((prevState) => {
          return {
            ...prevState,
            cards: data,
            cart: JSON.parse(localStorage.getItem('cart') || '[]'),
            favorite: JSON.parse(localStorage.getItem('favorite') || '[]')
          }
        });
      })
  }

  handlerModal = () => {
    this.setState(prevState => {
      return {
        ...prevState,
        isModal: !prevState.isModal,

      }
    })
  };
  handlerCurrentProd = (currentProd) => {
    this.setState((prevState) => {
      return {
        ...prevState,
        currentProd: { ...currentProd }
      }
    })
  }
  handlerCart = (currentProd) => {
    const isAddCart = this.state.cart.some(row => row.articul === currentProd.articul);
    if (!isAddCart) 
    {
      this.setState((prevState) => {
        const cartStorage = [...prevState.cart, currentProd];
        localStorage.setItem('cart', JSON.stringify(cartStorage))
        return {
          ...prevState,
          isModal: !prevState.isModal,
          cart: cartStorage
        }
      })
    } else this.handlerModal();
  }

  handlerFavorite = (heart) => {
    const isAdd = this.state.favorite.some(row => row.articul === heart.articul);
    let favorite;
    if (isAdd) {
      favorite = this.state.favorite.filter(row => row.articul != heart.articul);
    } else {
      favorite = [...this.state.favorite, heart];
    }
    localStorage.setItem('favorite', JSON.stringify(favorite))
    this.setState((prevState) => {

      return {
        ...prevState,
        favorite,
      }
    })
  }
  render() {
    const {
      isModal,
      favorite,
      currentProd,
      cart
    } = this.state;
    const style = "btn";
    const action = (
      <div className="button-wrapper">
        <button className={style} type="button" onClick={() => this.handlerCart(currentProd)} >OK</button>
        <button className="btn" type="button" onClick={() => this.handlerModal()}>Cancel</button>

      </div>)
    const cards = this.state.cards.map(row => {
      return {
        ...row,
        inFav: this.state.favorite.some(dataCard => dataCard.articul == row.articul),
      };
    });
    return (
      <>
        <div className="page__wrapper">
          <Header count={cart.length} countFav={favorite.length} />
          <main className="main">

            <AllCards data={cards} handlerCurrentProd={this.handlerCurrentProd} handlerModal={this.handlerModal} handlerFavorite={this.handlerFavorite} />
          </main>
          <Footer />
          {isModal && <Modal action={action} closeModal={this.handlerModal} title={currentProd.name} />}
        </div>

      </>
    )
  }
}
export default App;
